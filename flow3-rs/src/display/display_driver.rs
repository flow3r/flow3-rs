use embassy_time::Delay;
use gc9a01::{
    mode::BasicMode,
    prelude::{DisplayConfiguration, DisplayResolution240x240},
    rotation::DisplayRotation,
    Gc9a01,
};
use hal::{
    gdma::{Channel0, ChannelCreator0},
    gpio::{Gpio38, Gpio40, GpioPin, Output, PushPull, Unknown},
    peripherals::SPI2,
    spi::{
        dma::{SpiDma, WithDmaSpi2},
        FullDuplexMode,
    },
    Spi,
};

use super::display_interface::SpiDmaInterface;

pub type DisplayDriver<'a> = Gc9a01<
    SpiDmaInterface<
        SpiDma<'a, SPI2, Channel0, FullDuplexMode>,
        GpioPin<Output<PushPull>, 38>,
        GpioPin<Output<PushPull>, 40>,
    >,
    DisplayResolution240x240,
    BasicMode,
>;

pub async fn init_display_driver<'a>(
    spi: Spi<'static, SPI2, FullDuplexMode>,
    channel: ChannelCreator0,
    descriptors: &'a mut [u32; 27],
    rx_descriptors: &'a mut [u32; 27],
    dc: Gpio38<Unknown>,
    cs: Gpio40<Unknown>,
) -> DisplayDriver<'a> {
    let spi_dma = spi.with_dma(channel.configure(
        false,
        descriptors,
        rx_descriptors,
        hal::dma::DmaPriority::Priority0,
    ));

    let display_interface = SpiDmaInterface::new(
        spi_dma,
        dc.into_push_pull_output(),
        cs.into_push_pull_output(),
    );

    let mut display_driver = Gc9a01::new(
        display_interface,
        DisplayResolution240x240,
        DisplayRotation::Rotate180,
    );

    display_driver.init(&mut Delay).await.unwrap();

    display_driver
}
