#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_time::{Duration, Timer};
use esp_println::println;

use esp_backtrace as _;

use flow3_rs::Flow3r;
use flow3_rs_rt::start_runtime;
use hal::prelude::*;

#[entry]
fn runtime() -> ! {
    start_runtime(main)
}

#[embassy_executor::task]
async fn main(_flow3r: Flow3r) {
    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(task()).unwrap();
}

#[embassy_executor::task]
async fn task() {
    loop {
        println!("running!");
        Timer::after(Duration::from_secs(1)).await;
    }
}
